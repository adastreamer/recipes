class ApiController < ApplicationController
  def getIngredientInfo
    id = params[:id]
    e = Ingredient.find id
	obj = {}
    obj[:measure] = e.measure.nil? ? "" : "#{e.measure[:title]}"
	obj[:calories] = e['calories'].nil? ? "" : "#{e['calories']}"
	obj[:protein] = e['protein'].nil? ? "" : "#{e['protein']}"
	obj[:carbohydrate] = e['carbohydrate'].nil? ? "" : "#{e['carbohydrate']}"
	obj[:fat] = e['fat'].nil? ? "" : "#{e['fat']}"

#	e[:measuretitle] = e.measure[:title]
    render text: obj.to_json
  end
end
