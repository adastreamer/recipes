# == Schema Information
#
# Table name: recipe_recipe_types
#
#  id             :integer          not null, primary key
#  recipe_id      :integer
#  recipe_type_id :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class RecipeRecipeType < ActiveRecord::Base
  belongs_to :recipe
  belongs_to :recipe_type
end
