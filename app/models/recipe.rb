# == Schema Information
#
# Table name: recipes
#
#  id                 :integer          not null, primary key
#  title              :string(255)
#  recipe_type_id     :integer
#  portion_count      :integer
#  calories           :float
#  protein            :float
#  carbohydrate       :float
#  fat                :float
#  time               :integer
#  created_at         :datetime
#  updated_at         :datetime
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#

class Recipe < ActiveRecord::Base
  
  validates_presence_of :title, :recipe_type_id, :portion_count, :calories, :protein, :carbohydrate, :fat, :time, :photo
  
  has_many :recipe_ingredients
  accepts_nested_attributes_for :recipe_ingredients, :allow_destroy => true
  
  has_many :recipe_steps
  accepts_nested_attributes_for :recipe_steps, :allow_destroy => true
  
  has_many :recipe_tag
  has_many :tag, :through => :recipe_tag
  
  has_many :recipe_recipe_type
  has_many :recipe_type, :through => :recipe_recipe_type
  
  #belongs_to :recipe_type
  has_attached_file :photo
  validates_attachment_content_type :photo, :content_type => ["image/jpg", "image/jpeg", "image/png"]
  
end
