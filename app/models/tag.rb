# == Schema Information
#
# Table name: tags
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Tag < ActiveRecord::Base
  has_many :recipe_tag
  has_many :recipe, :through => :recipe_tag
  
  def name
    self.title
  end
  
end
