# == Schema Information
#
# Table name: recipe_tags
#
#  id         :integer          not null, primary key
#  tag_id     :integer
#  recipe_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class RecipeTag < ActiveRecord::Base
  belongs_to :tag
  belongs_to :recipe
  

  
end
