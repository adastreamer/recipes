# == Schema Information
#
# Table name: recipe_ingredients
#
#  id            :integer          not null, primary key
#  ingredient_id :integer
#  count         :float
#  recipe_id     :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class RecipeIngredient < ActiveRecord::Base
  belongs_to :ingredient
  belongs_to :recipe
  
  def title
    self.ingredient.nil? ? "" : "#{self.ingredient.title} - #{self.count} #{self.ingredient.measure.nil? ? '' : self.ingredient.measure.title}"
  end
  
end
