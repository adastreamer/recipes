# == Schema Information
#
# Table name: measures
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Measure < ActiveRecord::Base
  has_many :ingredient
end
