class Ability
  include CanCan::Ability

  def initialize(user)
    
    can :access, :rails_admin   # grant access to rails_admin
    can :dashboard              # grant access to the dashboard
    
    #can :manage, :all
    if user.role.eql? 'admin'
      #can :manage, [User]
      can :manage, :all

RailsAdmin.config do |config|

  config.model 'RecipeIngredient' do
    visible true
    edit do
      field :ingredient
      field :count
      field :recipe do
        visible do
          false
        end
      end
    end
  end
  
  config.model 'RecipeStep' do
    visible true
    edit do
      field :text
      field :recipe do
        visible do
          false
        end
      end
    end
  end  
  
  config.model 'RecipeTag' do
    visible true
    edit do
      field :recipe
      field :tag
    end
  end
    
  
  config.model 'RecipeType' do
    visible true
    edit do
      field :title
      field :recipe do
        visible do
          false
        end
      end
    end
  end
      
  config.model 'Tag' do
    visible true
    edit do
      field :title
      field :recipeTag do
        visible do
          false
        end
      end
    end
  end  

  config.model 'Recipe' do
    edit do

      group :main do
        label "Основное"
      end

      group :recipe_ingredients do
        label "Ингредиенты"
      end

      group :recipe_steps do
        label "Этапы приготовления"
      end

      group :tag do
        label "Тэги"
      end
    
      field :title do
        required true
        group :main
      end
      field :recipe_type do
        required true
        group :main
      end
      field :portion_count do
        required true
        group :main
      end
      field :calories do
        required true
        group :main
      end
      field :protein do
        required true
        group :main
      end
      field :carbohydrate do
        required true
        group :main
      end
      field :fat do
        required true
        group :main
      end
      field :time do
        required true
        group :main
      end
      field :photo do
        required true
        group :main
      end
      
      #if (current_user.id.eql? 2) then
      field :recipe_ingredients do
        required true
        group :recipe_ingredients
      end
      field :tag do
        required true
        group :tag
      end
      field :recipe_steps do
        required true
        group :recipe_steps
      end
    end
  end

  config.model 'User' do
    visible true
    edit do
      field :email
      field :password
      field :password_confirmation
      field :role, :enum do
        enum do
          ['admin', 'manager', 'manager Only Adding']
        end      
      end
    end
  end

  config.model 'Ingredient' do
    visible true
  end

end      
      
    end
     
    if user.role.eql? 'manager'
      can :read, [Recipe]
      can :manage, [Recipe]
      
      can :read, [RecipeType]
      can :manage, [Ingredient]
      
      can :manage, [RecipeIngredient]
      can :manage, [RecipeStep]
      can :manage, [Tag]
      can :manage, [RecipeTag]
      
RailsAdmin.config do |config|

  config.model 'RecipeIngredient' do
    visible false
    edit do
      field :ingredient
      field :count
      field :recipe do
        visible do
          false
        end
      end
    end
  end
  
  config.model 'RecipeStep' do
    visible false
    edit do
      field :text
      field :recipe do
        visible do
          false
        end
      end
    end
  end  
  
  config.model 'RecipeTag' do
    visible false
    edit do
      field :recipe
      field :tag
    end
  end
    
  
  config.model 'RecipeType' do
    visible false
    edit do
      field :title
      field :recipe do
        visible do
          false
        end
      end
    end
  end
      
  config.model 'Tag' do
    visible false
    edit do
      field :title
      field :recipeTag do
        visible do
          false
        end
      end
    end
  end  

  config.model 'Recipe' do
    edit do

      group :main do
        label "Основное"
      end

      group :recipe_ingredients do
        label "Ингредиенты"
      end

      group :recipe_steps do
        label "Этапы приготовления"
      end

      group :tag do
        label "Тэги"
      end
    
      field :title do
        required true
        group :main
      end
      field :recipe_type do
        required true
        group :main
      end
      #field :recipe_type do
      #  required true
      #  group :main
      #end
      field :portion_count do
        required true
        group :main
      end
      field :calories do
        required true
        group :main
      end
      field :protein do
        required true
        group :main
      end
      field :carbohydrate do
        required true
        group :main
      end
      field :fat do
        required true
        group :main
      end
      field :time do
        required true
        group :main
      end
      field :photo do
        required true
        group :main
      end
      
      #if (current_user.id.eql? 2) then
      field :recipe_ingredients do
        required true
        group :recipe_ingredients
      end
      field :tag do
        required true
        group :tag
      end
      field :recipe_steps do
        required true
        group :recipe_steps
      end
    end
  end

  config.model 'User' do
    visible false
    edit do
      field :email
      field :password
      field :password_confirmation
      field :role, :enum do
        enum do
          ['admin', 'manager', 'manager Only Adding']
        end      
      end
    end
  end

  config.model 'Ingredient' do
    visible true
  end

end      
      
      
    end
    
    if user.role.eql? 'manager Only Adding'
      can :read, [Recipe]
      can :new, [Recipe]
      
      can :read, [RecipeType]
      can :read, [Ingredient]
      can :new, [Ingredient]
      
      can :manage, [RecipeIngredient]
      can :manage, [RecipeStep]
      can :manage, [Tag]
      can :manage, [RecipeTag]
      
RailsAdmin.config do |config|

  config.model 'RecipeIngredient' do
    visible false
    edit do
      field :ingredient
      field :count
      field :recipe do
        visible do
          false
        end
      end
    end
  end
  
  config.model 'RecipeStep' do
    visible false
    edit do
      field :text
      field :recipe do
        visible do
          false
        end
      end
    end
  end  
  
  config.model 'RecipeTag' do
    visible false
    edit do
      field :recipe
      field :tag
    end
  end
    
  
  config.model 'RecipeType' do
    visible false
    edit do
      field :title
      field :recipe do
        visible do
          false
        end
      end
    end
  end
      
  config.model 'Tag' do
    visible false
    edit do
      field :title
      field :recipeTag do
        visible do
          false
        end
      end
    end
  end  

  config.model 'Recipe' do
    edit do

      group :main do
        label "Основное"
      end

      group :recipe_ingredients do
        label "Ингредиенты"
      end

      group :recipe_steps do
        label "Этапы приготовления"
      end

      group :tag do
        label "Тэги"
      end
    
      field :title do
        required true
        group :main
      end
      field :recipe_type do
        required true
        group :main
      end
      field :portion_count do
        required true
        group :main
      end
      field :calories do
        required true
        group :main
      end
      field :protein do
        required true
        group :main
      end
      field :carbohydrate do
        required true
        group :main
      end
      field :fat do
        required true
        group :main
      end
      field :time do
        required true
        group :main
      end
      field :photo do
        required true
        group :main
      end
      
      #if (current_user.id.eql? 2) then
      field :recipe_ingredients do
        required true
        group :recipe_ingredients
      end
      field :tag do
        required true
        group :tag
      end
      field :recipe_steps do
        required true
        group :recipe_steps
      end
      #end
    end
  end

  config.model 'User' do
    visible false
    edit do
      field :email
      field :password
      field :password_confirmation
      field :role, :enum do
        enum do
          ['admin', 'manager', 'manager Only Adding']
        end      
      end
    end
  end

  config.model 'Ingredient' do
    visible true
  end

end      
      
      
    end
    
    
    #require "roles/#{user.role}_.rb"
    
    
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
