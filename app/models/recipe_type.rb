# == Schema Information
#
# Table name: recipe_types
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class RecipeType < ActiveRecord::Base
  
  
  has_many :recipe_recipe_type
  has_many :recipe, :through => :recipe_recipe_type
end
