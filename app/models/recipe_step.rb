# == Schema Information
#
# Table name: recipe_steps
#
#  id         :integer          not null, primary key
#  recipe_id  :integer
#  text       :text
#  created_at :datetime
#  updated_at :datetime
#

class RecipeStep < ActiveRecord::Base
  belongs_to :recipe
  
  def name
    "#{self.text}"
  end
  
end
