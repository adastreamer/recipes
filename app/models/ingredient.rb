# == Schema Information
#
# Table name: ingredients
#
#  id                     :integer          not null, primary key
#  title                  :string(255)
#  measure_id             :integer
#  count_gram             :integer
#  calories               :float
#  protein                :float
#  carbohydrate           :float
#  fat                    :float
#  sugar                  :float
#  fibre                  :float
#  sodium                 :float
#  water                  :float
#  mono_and_disaccharides :float
#  fiber                  :float
#  organic_acid           :float
#  ash                    :float
#  potassium              :float
#  calcium                :float
#  magnesium              :float
#  phosphorus             :float
#  iron                   :float
#  vitamin_b              :float
#  vitamin_c              :float
#  vitamin_b1             :float
#  vitamin_b2             :float
#  vitamin_pp             :float
#  created_at             :datetime
#  updated_at             :datetime
#

class Ingredient < ActiveRecord::Base
  belongs_to :measure
end
