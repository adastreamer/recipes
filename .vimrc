:nmap <F1> :rew <Enter>
:nmap <F12> :n <Enter>
set nowrap
set tabstop=4
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
hi Directory ctermfg=3 ctermbg=4
hi comment ctermfg=3 ctermbg=4
