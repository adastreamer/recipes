# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140422202119) do

  create_table "ingredients", force: true do |t|
    t.string   "title"
    t.integer  "measure_id"
    t.integer  "count_gram"
    t.float    "calories"
    t.float    "protein"
    t.float    "carbohydrate"
    t.float    "fat"
    t.float    "sugar"
    t.float    "fibre"
    t.float    "sodium"
    t.float    "water"
    t.float    "mono_and_disaccharides"
    t.float    "fiber"
    t.float    "organic_acid"
    t.float    "ash"
    t.float    "potassium"
    t.float    "calcium"
    t.float    "magnesium"
    t.float    "phosphorus"
    t.float    "iron"
    t.float    "vitamin_b"
    t.float    "vitamin_c"
    t.float    "vitamin_b1"
    t.float    "vitamin_b2"
    t.float    "vitamin_pp"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "measures", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rails_admin_histories", force: true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      limit: 2
    t.integer  "year",       limit: 5
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], name: "index_rails_admin_histories"

  create_table "recipe_ingredients", force: true do |t|
    t.integer  "ingredient_id"
    t.float    "count"
    t.integer  "recipe_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "recipe_recipe_types", force: true do |t|
    t.integer  "recipe_id"
    t.integer  "recipe_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "recipe_steps", force: true do |t|
    t.integer  "recipe_id"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "recipe_tags", force: true do |t|
    t.integer  "tag_id"
    t.integer  "recipe_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "recipe_types", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "recipes", force: true do |t|
    t.string   "title"
    t.integer  "recipe_type_id"
    t.integer  "portion_count"
    t.float    "calories"
    t.float    "protein"
    t.float    "carbohydrate"
    t.float    "fat"
    t.integer  "time"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "tags", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
