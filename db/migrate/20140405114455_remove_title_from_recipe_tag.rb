class RemoveTitleFromRecipeTag < ActiveRecord::Migration
  def change
    remove_column :recipe_tags, :title
  end
end
