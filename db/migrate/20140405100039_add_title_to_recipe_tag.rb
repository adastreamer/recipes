class AddTitleToRecipeTag < ActiveRecord::Migration
  def change
    add_column :recipe_tags, :title, :string
  end
end
