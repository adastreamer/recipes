class CreateRecipeRecipeTypes < ActiveRecord::Migration
  def change
    create_table :recipe_recipe_types do |t|
      t.integer :recipe_id
      t.integer :recipe_type_id

      t.timestamps
    end
  end
end
