class CreateIngredients < ActiveRecord::Migration
  def change
    create_table :ingredients do |t|
      t.string :title
      t.integer :measure_id
      t.integer :count_gram
      t.float :calories
      t.float :protein
      t.float :carbohydrate
      t.float :fat
      t.float :sugar
      t.float :fibre
      t.float :sodium
      t.float :water
      t.float :mono_and_disaccharides
      t.float :fiber
      t.float :organic_acid
      t.float :ash
      t.float :potassium
      t.float :calcium
      t.float :magnesium
      t.float :phosphorus
      t.float :iron
      t.float :vitamin_b
      t.float :vitamin_c
      t.float :vitamin_b1
      t.float :vitamin_b2
      t.float :vitamin_pp

      t.timestamps
    end
  end
end
