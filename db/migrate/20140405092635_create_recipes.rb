class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :title
      t.integer :recipe_type_id
      t.integer :portion_count
      t.float :calories
      t.float :protein
      t.float :carbohydrate
      t.float :fat
      t.integer :time

      t.timestamps
    end
  end
end
