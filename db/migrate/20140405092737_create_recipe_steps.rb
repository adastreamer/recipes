class CreateRecipeSteps < ActiveRecord::Migration
  def change
    create_table :recipe_steps do |t|
      t.integer :recipe_id
      t.text :text

      t.timestamps
    end
  end
end
